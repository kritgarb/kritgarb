### Hi there, i am Garb Krit!

- 🔭 I’m currently working with backend
- 🌱 I’m currently learning data science
- 😄 Pronouns: him his
- ⚡ Fun fact: I'm colorblind and I'm developing a project that helps
 people to better understand colorblindness

<div align="center">
  <a href="https://github.com/kritgarb">
  <img height="180em" src="https://github-readme-stats.vercel.app/api?username=kritgarb&show_icons=true&theme=merko&include_all_commits=true&count_private=true"/>
  <img height="180em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=rafaballerini&layout=compact&langs_count=7&theme=merko"/>
</div>
  
<div style="display: inline_block"><br>
  <img align="center" alt="Garb-Python" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg">
  <img align="center" alt="Garb-Js" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-plain.svg">
  <img align="center" alt="Garb-HTML" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original.svg">
  <img align="center" alt="Garb-CSS" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original.svg">
</div>
  
  ##  <a href="https://picasion.com/"><img src="https://i.picasion.com/pic92/fdde3a0d9730ae05e420525dcb93bd61.gif" width="200" height="200" border="0" alt="https://picasion.com/" /></a><br />
 
<div> 
  <a href="https://instagram.com/kritgarb" target="_blank"><img src="https://img.shields.io/badge/-Instagram-%23E4405F?style=for-the-badge&logo=instagram&logoColor=white" target="_blank"></a>
 <a href="https://discord.gg/QbAChGbV" target="_blank"><img src="https://img.shields.io/badge/Discord-7289DA?style=for-the-badge&logo=discord&logoColor=white" target="_blank"></a> 
  <a href = "mailto:garbkrit@gmail.com"><img src="https://img.shields.io/badge/-Gmail-%23333?style=for-the-badge&logo=gmail&logoColor=white" target="_blank"></a>
  <a href="https://www.linkedin.com/in/garbkrit" target="_blank"><img src="https://img.shields.io/badge/-LinkedIn-%230077B5?style=for-the-badge&logo=linkedin&logoColor=white" target="_blank"></a> 
 
  
</div>
